# Using pre-commits with .legacy hooks

[This comment](https://github.com/pre-commit/pre-commit/issues/450#issuecomment-405616722)
mention this:

> specifically for user specific hooks, you could take advantage of the "migration mode" for pre-commit (the docs don't discuss the implementation details but they're not changing any time soon: pre-commit will invoke a hook by the name of $HOOK_TYPE.legacy if it exists. With this you could set up a shell script that invokes pre-commit run --config ~/my-config.yaml or some sort.

So the easiest solution is to add `.legacy` hooks that will get run as well.

## Usage

Add a symlink to your specific repo which point to this repos `./pre-commit.legacy`:

```bash
ln -s PATH_TO_THIS_REPO_CHECKOUT/pre-commit.legacy .git/config/pre-commit.legacy
```

In case you also want to add other hook stages, i.e. `commit-msg`.
*Beware: If `.git/config/commit-msg` doesn't exist, adding
`commit-msg.legacy` would not be run, so we install the hook it like this:*

```bash
ln -s PATH_TO_THIS_REPO_CHECKOUT/pre-commit.legacy .git/config/commit-msg
```

## Example output

```shell
Running custom hook stage: pre-commit (/home/varac/projects/git/pre-commit/example-repo/.git/hooks/pre-commit.legacy, args: ""):
local-test-hook..........................................................Passed

Now continuing with regular project-specific pre-commit hooks in /home/varac/projects/git/pre-commit/example-repo:
Trim Trailing Whitespace.................................................Passed
Fix End of Files.........................................................Passed
Check Yaml...............................................................Passed
Check for added large files..............................................Passed

Running custom hook stage: commit-msg (.git/hooks/commit-msg, args: ".git/COMMIT_EDITMSG"):
Conventional Commit......................................................Failed
- hook id: conventional-pre-commit
- exit code: 1

[Commit message] test

Your commit message does not follow Conventional Commits formatting
https://www.conventionalcommits.org/
...
```
