# Varac's common pre-commit config

**Currently this is rather a proof of concept than any useful hooks,
consider it work in progress!**

This repo hosts my custom, additional pre-commit config I like to include
in all my repos, additional to the `pre-commit-config.yaml` hosted in the
specifc project.

Currently, there's [no official way of including additional commits](https://github.com/pre-commit/pre-commit/issues/2337)
since the pre-commit maintainer doesn't want to support it. There are, however
[different ways to hack around this limitation](https://github.com/pre-commit/pre-commit/issues/450#issuecomment-405616722).

One of which is the use of legacy pre-commit hooks, which is documented in
`./legacy-hooks/README.md`. This option has it's downsides so my favorite
approach is to use a [global git core.hookpath](https://git-scm.com/docs/githooks),
inspired by [this nice blog
post](https://medium.com/@ripoche.b/using-global-pre-commit-hook-to-prevent-committing-unwanted-code-edbbf957ad12).

## Usage

Configue your global git `core.hooksPath` like this:

```sh
git config --global core.hooksPath ~/projects/git/pre-commit/pre-commit-config-varac
```

Then chdir into a different repository and install the pre-commit hooks

* [core.hooksPath being set makes it not possible to install](https://github.com/pre-commit/pre-commit/issues/1298)

try to commit sth, an example output:

```sh
❯ echo $(date) > hi.txt && ga . && gc -m'feat: add foo'

Executing custom hook (stage: pre-commit, file:
  /home/varac/projects/git/pre-commit/pre-commit-config-varac/pre-commit, args: ""):
Running custom hook /home/varac/projects/git/pre-commit/pre-commit-config-varac/.git/hooks/pre-commit
local-test-hook..........................................................Passed

Now continuing with regular project-specific pre-commit hook in /home/varac/projects/git/pre-commit/global-hooks-repo-test:
Trim Trailing Whitespace.................................................Passed
Fix End of Files.........................................................Passed
Check Yaml...........................................(no files to check)Skipped
Check for added large files..............................................Passed

Executing custom hook (stage: commit-msg, file:
  /home/varac/projects/git/pre-commit/pre-commit-config-varac/commit-msg, args: ".git/COMMIT_EDITMSG"):
Running custom hook /home/varac/projects/git/pre-commit/pre-commit-config-varac/.git/hooks/commit-msg
Conventional Commit......................................................Passed

No local commit-msg hook found in /home/varac/projects/git/pre-commit/global-hooks-repo-test.
[main b9482c5] feat: add foo
 1 file changed, 1 insertion(+), 1 deletion(-)
```
